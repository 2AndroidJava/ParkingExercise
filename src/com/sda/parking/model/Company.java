package com.sda.parking.model;

import java.util.ArrayList;

public class Company {
	private CarRegistry registry;
	private ArrayList<Parking> parkings;
	private ArrayList<CashRegister> cashRegisters;

	public Company() {
		parkings = new ArrayList<>();
		registry = new CarRegistry();
		cashRegisters = new ArrayList<>();
	}

	/**
	 * Dodawanie parkingu. Po dodaniu wypisuje komunikat o numerze parkingu.
	 * Ponieważ parkingi iterowane są od 0, a interesuje mnie numer ostatnio
	 * dodanego parkingu, to wypisanie musi być parkings.size()-1.
	 */
	public void addParking() {
		parkings.add(new Parking());
		System.out.println("Dodano parking: " + (parkings.size() - 1));
	}

	public void addCar(int parkingId) {
		if (parkings.size() <= parkingId) {
			System.err.println("Parking nie istnieje.");
			return;
		}
		
		parkings.get(parkingId).addCar();
	}

	public void carEntry(int parkingId) {
		if (parkings.size() <= parkingId) {
			System.err.println("Parking nie istnieje.");
			return;
		}
		
		String ticketId = parkings.get(parkingId).carEntry();
		registry.addToRegistry(ticketId);
	}

	public void carLeave(String carId) {
		System.out.println("Samochod " + carId + " proba wyjazdu.");

		if (registry.checkTicket(carId)) {
			registry.carLeave(carId);
		} else {
			System.err.println("Ticket nie został zwalidowany.");
		}
	}

	public void addCashRegister() {
		cashRegisters.add(new CashRegister());
		System.out.println("Dodano kasę: " + (parkings.size() - 1));
	}

	public void validateTicket(int cashRegistryId, String ticketId) {
		System.out.println("Próba walidacji ticketu " + ticketId);

		if (cashRegisters.size() <= cashRegistryId) {
			System.err.println("Kasa nie istnieje.");
			return;
		}

		if (registry.checkTicket(ticketId)) {
			System.err.println("Ticket był już zwalidowany.");
		} else {
			ParkingInformation information = registry.validateTicket(cashRegistryId, ticketId);
			cashRegisters.get(cashRegistryId).validateTicket(information);
		}
	}
}

package com.sda.parking.model;

import java.util.HashMap;
import java.util.Map;

public class CarRegistry {

	private Map<String, ParkingInformation> registry;

	public CarRegistry() {
		registry = new HashMap<>();
	}

	/**
	 * Po wjeździe samochodu na parking dodajemy informacje o jego parkowaniu do
	 * rejestru (ticket id zostaje wstawiony do mapy)
	 * 
	 * @param ticketId
	 *            - numer nadanego ticketu.
	 */
	public void addToRegistry(String ticketId) {
		registry.put(ticketId, new ParkingInformation(ticketId));

		System.out.println("Dodano " + ticketId + " do rejestru.");
	}

	public boolean checkTicket(String carId) {
		return registry.get(carId).isValidated();
	}

	public void carLeave(String carId) {
		registry.get(carId).leave();
		System.out.println("Samochód " + carId + " opuszcza parking.");
	}

	/**
	 * Wynikiem walidacji ticketu jest wpisanie flagi validated oraz zwrócenie parking ticket.
	 * @param cashRegistryId - numer kasy
	 * @param ticketId - numer ticketu
	 * @return parking information - dane o tickecie.
	 */
	public ParkingInformation validateTicket(int cashRegistryId, String ticketId) {
		registry.get(ticketId).validateTicket(cashRegistryId);
		System.out.println("Ticket " + ticketId + "został zwalidowany w kasie " + cashRegistryId);

		return registry.get(ticketId);
	}
}

package com.sda.parking.model;

public class ParkingInformation {
	private String ticketId;
	private Long startTimestamp;
	private Long leaveTimestamp;

	// numer kasy w której rozliczono się
	private int cashRegistryId;

	private boolean validated = false;

	public ParkingInformation(String ticketId) {
		this.ticketId = ticketId;

		this.startTimestamp = System.currentTimeMillis();
		this.leaveTimestamp = null;
	}

	/**
	 * Rozliczamy ticket.
	 * 
	 * @param cashRegistry
	 *            - numer kasy w której się rozliczyliśmy.
	 */
	public void validateTicket(int cashRegistry) {
		validated = true;
		cashRegistryId = cashRegistry;

		System.out.println("Ticket " + ticketId + " rozliczony w kasie " + cashRegistry);
	}

	public boolean isValidated() {
		return validated;
	}

	public void leave() {
		this.leaveTimestamp = System.currentTimeMillis();
	}
}
